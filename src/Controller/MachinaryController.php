<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class MachinaryController extends AbstractController
{
    /**
     * @Route("/machinary", name="machinary")
     */
    public function index()
    {
        return $this->render('machinary/index.html.twig', [
            'controller_name' => 'MachinaryController',
        ]);
    }
}
