<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DisabilityController extends AbstractController
{
    /**
     * @Route("/disability", name="disability")
     */
    public function index()
    {
        return $this->render('disability/index.html.twig', [
            'controller_name' => 'DisabilityController',
        ]);
    }
}
