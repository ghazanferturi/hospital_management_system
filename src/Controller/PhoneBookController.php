<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class PhoneBookController extends AbstractController
{
    /**
     * @Route("/phone/book", name="phone_book")
     */
    public function index()
    {
        return $this->render('phone_book/index.html.twig', [
            'controller_name' => 'PhoneBookController',
        ]);
    }
}
