<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class FinancialController extends AbstractController
{
    /**
     * @Route("/financial", name="financial")
     */
    public function index()
    {
        return $this->render('financial/index.html.twig', [
            'controller_name' => 'FinancialController',
        ]);
    }
}
