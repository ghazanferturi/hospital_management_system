<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ReceiptRepository")
 */
class Receipt
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $recipientDate;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $deliveryDate;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Article", inversedBy="receipts")
     * @ORM\JoinColumn(name="article_id", referencedColumnName="id")
     */
    private $article;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Machine", inversedBy="receipts")
     * @ORM\JoinColumn(name="machine_id", referencedColumnName="id")
     */
    private $machine;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Employee", inversedBy="receipts")
     * @ORM\JoinColumn(name="employee_id", referencedColumnName="id")
     */
    private $employee;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getRecipientDate(): string
    {
        return $this->recipientDate ?? '';
    }

    /**
     * @param string $recipientDate
     */
    public function setRecipientDate(string $recipientDate): void
    {
        $this->recipientDate = $recipientDate;
    }

    /**
     * @return string
     */
    public function getDeliveryDate(): string
    {
        return $this->deliveryDate ?? '';
    }

    /**
     * @param string $deliveryDate
     */
    public function setDeliveryDate(string $deliveryDate): void
    {
        $this->deliveryDate = $deliveryDate;
    }

    /**
     * @return mixed
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * @param mixed $article
     */
    public function setArticle($article): void
    {
        $this->article = $article;
    }

    /**
     * @return mixed
     */
    public function getMachine()
    {
        return $this->machine;
    }

    /**
     * @param mixed $machine
     */
    public function setMachine($machine): void
    {
        $this->machine = $machine;
    }

    /**
     * @return mixed
     */
    public function getEmployee()
    {
        return $this->employee;
    }

    /**
     * @param mixed $employee
     */
    public function setEmployee($employee): void
    {
        $this->employee = $employee;
    }
}
