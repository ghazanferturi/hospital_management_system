<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ArticleRepository")
 */
class Article
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $date;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Patient", inversedBy="article")
     * @ORM\JoinColumn(name="patient_id", referencedColumnName="id")
     */
    private $patient;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Transfer", inversedBy="article")
     * @ORM\JoinColumn(name="transfer_id", referencedColumnName="id")
     */
    private $transfer;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Machine", inversedBy="article")
     * @ORM\JoinColumn(name="machine_id", referencedColumnName="id")
     */
    private $machine;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Department", inversedBy="article")
     * @ORM\JoinColumn(name="department_id", referencedColumnName="id")
     */
    private $department;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Disability", mappedBy="article")
     */
    private $disability;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\FinancialAid", mappedBy="article")
     */
    private $financialAid;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Receipt", mappedBy="article")
     */
    private $receipts;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price ?? 0.0;
    }

    /**
     * @param float $price
     */
    public function setPrice(float $price): void
    {
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getDate(): string
    {
        return $this->date ?? '';
    }

    /**
     * @param string $date
     */
    public function setDate(string $date): void
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getPatient()
    {
        return $this->patient;
    }

    /**
     * @param mixed $patient
     */
    public function setPatient($patient): void
    {
        $this->patient = $patient;
    }

    /**
     * @return mixed
     */
    public function getTransfer()
    {
        return $this->transfer;
    }

    /**
     * @param mixed $transfer
     */
    public function setTransfer($transfer): void
    {
        $this->transfer = $transfer;
    }

    /**
     * @return mixed
     */
    public function getMachine()
    {
        return $this->machine;
    }

    /**
     * @param mixed $machine
     */
    public function setMachine($machine): void
    {
        $this->machine = $machine;
    }

    /**
     * @return mixed
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * @param mixed $department
     */
    public function setDepartment($department): void
    {
        $this->department = $department;
    }

    /**
     * @return mixed
     */
    public function getDisability()
    {
        return $this->disability;
    }

    /**
     * @param mixed $disability
     */
    public function setDisability($disability): void
    {
        $this->disability = $disability;
    }

    /**
     * @return mixed
     */
    public function getFinancialAid()
    {
        return $this->financialAid;
    }

    /**
     * @param mixed $financialAid
     */
    public function setFinancialAid($financialAid): void
    {
        $this->financialAid = $financialAid;
    }

    /**
     * @return mixed
     */
    public function getReceipts()
    {
        return $this->receipts;
    }

    /**
     * @param mixed $receipts
     */
    public function setReceipts($receipts): void
    {
        $this->receipts = $receipts;
    }

}
