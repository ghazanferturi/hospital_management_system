<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MachineRepository")
 */
class Machine
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=150)
     */
    private $arName;

    /**
     * @ORM\Column(type="string", length=150)
     */
    private $enName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Article", mappedBy="machine")
     */
    private $article;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\MachineType", inversedBy="machine")
     * @ORM\JoinColumn(name="machineType_id", referencedColumnName="id")
     */
    private $machineType;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Receipt", mappedBy="machine")
     */
    private $receipts;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getArName(): string
    {
        return $this->arName ?? '';
    }

    /**
     * @param string $arName
     */
    public function setArName(string $arName): void
    {
        $this->arName = $arName;
    }

    /**
     * @return string
     */
    public function getEnName(): string
    {
        return $this->enName ?? '';
    }

    /**
     * @param string $enName
     */
    public function setEnName(string $enName): void
    {
        $this->enName = $enName;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description ?? '';
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getMachineType()
    {
        return $this->machineType;
    }

    /**
     * @param mixed $machineType
     */
    public function setMachineType($machineType): void
    {
        $this->machineType = $machineType;
    }

    /**
     * @return mixed
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * @param mixed $article
     */
    public function setArticle($article): void
    {
        $this->article = $article;
    }

    /**
     * @return mixed
     */
    public function getReceipts()
    {
        return $this->receipts;
    }

    /**
     * @param mixed $receipts
     */
    public function setReceipts($receipts): void
    {
        $this->receipts = $receipts;
    }

}
