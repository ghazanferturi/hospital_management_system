<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PatientRepository")
 */
class Patient
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $middleName;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=15)
     */
    private $civilId;

    /**
     * @ORM\Column(type="string", length=15)
     */
    private $passportNumber;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $image;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $notes;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $dateOfBirth;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $creationTime;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $modificationTime;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Gender", inversedBy="patient")
     * @ORM\JoinColumn(name="gender_id", referencedColumnName="id")
     */
    private $gender;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Nationality", inversedBy="patient")
     * @ORM\JoinColumn(name="nationality_id", referencedColumnName="id")
     */
    private $nationality;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\NationalityType", inversedBy="patient")
     * @ORM\JoinColumn(name="nationalityType_id",  referencedColumnName="id")
     */
    private $nationalityType;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Article", mappedBy="patient")
     */
    private $article;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName ?? '';
    }

    /**
     * @param string $firstName
     */
    public function setFirstName(string $firstName): void
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getMiddleName(): string
    {
        return $this->middleName ?? '';
    }

    /**
     * @param string $middleName
     */
    public function setMiddleName(string $middleName): void
    {
        $this->middleName = $middleName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName ?? '';
    }

    /**
     * @param string $lastName
     */
    public function setLastName(string $lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getCivilId(): string
    {
        return $this->civilId ??  '';
    }

    /**
     * @param string $civilId
     */
    public function setCivilId(string $civilId): void
    {
        $this->civilId = $civilId;
    }

    /**
     * @return string
     */
    public function getPassportNumber(): string
    {
        return $this->passportNumber ?? '';
    }

    /**
     * @param string $passportNumber
     */
    public function setPassportNumber(string $passportNumber): void
    {
        $this->passportNumber = $passportNumber;
    }

    /**
     * @return string
     */
    public function getImage(): string
    {
        return $this->image ?? '';
    }

    /**
     * @param string $image
     */
    public function setImage(string $image): void
    {
        $this->image = $image;
    }

    /**
     * @return string
     */
    public function getDateOfBirth(): string
    {
        return $this->dateOfBirth ?? 'Y-m-d H:i:s';
    }

    /**
     * @param string $dateOfBirth
     */
    public function setDateOfBirth(string $dateOfBirth): void
    {
        $this->dateOfBirth = $dateOfBirth;
    }

    /**
     * @return string
     */
    public function getCreationTime(): string
    {
        return $this->creationTime ?? 'Y-m-d H:i:s';
    }

    /**
     * @param mixed $creationTime
     */
    public function setCreationTime(string $creationTime): void
    {
        $this->creationTime = $creationTime;
    }

    /**
     * @return string
     */
    public function getModificationTime(): string
    {
        return $this->modificationTime ?? 'Y-m-d H:i:s';
    }

    /**
     * @param mixed $modificationTime
     */
    public function setModificationTime(string $modificationTime): void
    {
        $this->modificationTime = $modificationTime;
    }

    /**
     * @return string
     */
    public function getNotes(): string
    {
        return $this->notes ?? '';
    }

    /**
     * @param string $notes
     */
    public function setNotes(string $notes): void
    {
        $this->notes = $notes;
    }

    /**
     * @return mixed
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param mixed $gender
     */
    public function setGender($gender): void
    {
        $this->gender = $gender;
    }

    /**
     * @return mixed
     */
    public function getNationality()
    {
        return $this->nationality;
    }

    /**
     * @param mixed $nationality
     */
    public function setNationality($nationality): void
    {
        $this->nationality = $nationality;
    }

    /**
     * @return mixed
     */
    public function getNationalityType()
    {
        return $this->nationalityType;
    }

    /**
     * @param mixed $nationalityType
     */
    public function setNationalityType($nationalityType): void
    {
        $this->nationalityType = $nationalityType;
    }

    /**
     * @return mixed
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * @param mixed $article
     */
    public function setArticle($article): void
    {
        $this->article = $article;
    }

}
