<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PhoneNumberRepository")
 */
class PhoneNumber
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $number;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $typeDiscriminator;

    /**
     * @ORM\Column(type="integer")
     */
    private $phoneOfId;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNumber(): string
    {
        return $this->number ?? '';
    }

    /**
     * @param string $number
     */
    public function setNumber(string $number): void
    {
        $this->number = $number;
    }

    /**
     * @return string
     */
    public function getTypeDiscriminator(): string
    {
        return $this->typeDiscriminator ?? '';
    }

    /**
     * @param string $typeDiscriminator
     */
    public function setTypeDiscriminator(string $typeDiscriminator): void
    {
        $this->typeDiscriminator = $typeDiscriminator;
    }

    /**
     * @return int
     */
    public function getPhoneOfId(): int
    {
        return $this->phoneOfId ?? 0;
    }

    /**
     * @param int $phoneOfId
     */
    public function setPhoneOfId(int $phoneOfId): void
    {
        $this->phoneOfId = $phoneOfId;
    }
}
