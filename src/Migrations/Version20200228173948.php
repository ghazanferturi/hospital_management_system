<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200228173948 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE nationality_type (id INT AUTO_INCREMENT NOT NULL, type VARCHAR(20), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE financial_aid (id INT AUTO_INCREMENT NOT NULL, article_id INT DEFAULT NULL, description VARCHAR(255) NOT NULL, amount DOUBLE PRECISION NOT NULL, date VARCHAR(20) NOT NULL, UNIQUE INDEX UNIQ_4704BE4F7294869C (article_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE patient (id INT AUTO_INCREMENT NOT NULL, gender_id INT DEFAULT NULL, nationality_id INT DEFAULT NULL, first_name VARCHAR(100) NOT NULL, middle_name VARCHAR(100) NOT NULL, last_name VARCHAR(100) NOT NULL, civil_id VARCHAR(15) NOT NULL, passport_number VARCHAR(15) NOT NULL, image VARCHAR(255) NOT NULL, notes VARCHAR(255) NOT NULL, date_of_birth VARCHAR(100) NOT NULL, creation_time VARCHAR(100) NOT NULL, modification_time VARCHAR(100) NOT NULL, nationalityType_id INT DEFAULT NULL, UNIQUE INDEX UNIQ_1ADAD7EB708A0E0 (gender_id), UNIQUE INDEX UNIQ_1ADAD7EB1C9DA55 (nationality_id), UNIQUE INDEX UNIQ_1ADAD7EB31FFB0CB (nationalityType_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE employee (id INT AUTO_INCREMENT NOT NULL, department_id INT DEFAULT NULL, gender_id INT DEFAULT NULL, nationality_id INT DEFAULT NULL, first_name VARCHAR(100) NOT NULL, middle_name VARCHAR(100) NOT NULL, last_name VARCHAR(100) NOT NULL, civil_id VARCHAR(15) NOT NULL, passport_number VARCHAR(15) NOT NULL, image VARCHAR(255) NOT NULL, date_of_birth VARCHAR(100) NOT NULL, hire_date VARCHAR(100) NOT NULL, creation_time VARCHAR(100) NOT NULL, modification_time VARCHAR(100) NOT NULL, nationalityType_id INT DEFAULT NULL, positionRoles_id INT DEFAULT NULL, UNIQUE INDEX UNIQ_5D9F75A1AE80F5DF (department_id), UNIQUE INDEX UNIQ_5D9F75A1708A0E0 (gender_id), UNIQUE INDEX UNIQ_5D9F75A11C9DA55 (nationality_id), UNIQUE INDEX UNIQ_5D9F75A131FFB0CB (nationalityType_id), UNIQUE INDEX UNIQ_5D9F75A12608C31C (positionRoles_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE area (id INT AUTO_INCREMENT NOT NULL, city_id INT DEFAULT NULL, name VARCHAR(100) NOT NULL, INDEX IDX_D7943D688BAC62AF (city_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE gender (id INT AUTO_INCREMENT NOT NULL, gender VARCHAR(15) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE nationality (id INT AUTO_INCREMENT NOT NULL, nationality VARCHAR(100) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE machine (id INT AUTO_INCREMENT NOT NULL, ar_name VARCHAR(150) NOT NULL, en_name VARCHAR(150) NOT NULL, description VARCHAR(255) NOT NULL, machineType_id INT DEFAULT NULL, UNIQUE INDEX UNIQ_1505DF843AE7FDC9 (machineType_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE receipt (id INT AUTO_INCREMENT NOT NULL, article_id INT DEFAULT NULL, machine_id INT DEFAULT NULL, employee_id INT DEFAULT NULL, recipient_date VARCHAR(20) NOT NULL, delivery_date VARCHAR(20) NOT NULL, UNIQUE INDEX UNIQ_5399B6457294869C (article_id), UNIQUE INDEX UNIQ_5399B645F6B75B26 (machine_id), UNIQUE INDEX UNIQ_5399B6458C03F15C (employee_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE position_role (id INT AUTO_INCREMENT NOT NULL, role VARCHAR(150) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE transfer (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(150) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE machine_type (id INT AUTO_INCREMENT NOT NULL, type VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE department (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(150) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE phone_number (id INT AUTO_INCREMENT NOT NULL, number VARCHAR(20) NOT NULL, type_discriminator VARCHAR(255) NOT NULL, phone_of_id INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE article (id INT AUTO_INCREMENT NOT NULL, patient_id INT DEFAULT NULL, transfer_id INT DEFAULT NULL, machine_id INT DEFAULT NULL, department_id INT DEFAULT NULL, price DOUBLE PRECISION NOT NULL, date VARCHAR(100) NOT NULL, UNIQUE INDEX UNIQ_23A0E666B899279 (patient_id), UNIQUE INDEX UNIQ_23A0E66537048AF (transfer_id), UNIQUE INDEX UNIQ_23A0E66F6B75B26 (machine_id), UNIQUE INDEX UNIQ_23A0E66AE80F5DF (department_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE city (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(100) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE disability (id INT AUTO_INCREMENT NOT NULL, article_id INT DEFAULT NULL, description VARCHAR(255) NOT NULL, amount DOUBLE PRECISION NOT NULL, date VARCHAR(20) NOT NULL, UNIQUE INDEX UNIQ_413A5CB27294869C (article_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE financial_aid ADD CONSTRAINT FK_4704BE4F7294869C FOREIGN KEY (article_id) REFERENCES article (id)');
        $this->addSql('ALTER TABLE patient ADD CONSTRAINT FK_1ADAD7EB708A0E0 FOREIGN KEY (gender_id) REFERENCES gender (id)');
        $this->addSql('ALTER TABLE patient ADD CONSTRAINT FK_1ADAD7EB1C9DA55 FOREIGN KEY (nationality_id) REFERENCES nationality (id)');
        $this->addSql('ALTER TABLE patient ADD CONSTRAINT FK_1ADAD7EB31FFB0CB FOREIGN KEY (nationalityType_id) REFERENCES nationality_type (id)');
        $this->addSql('ALTER TABLE employee ADD CONSTRAINT FK_5D9F75A1AE80F5DF FOREIGN KEY (department_id) REFERENCES department (id)');
        $this->addSql('ALTER TABLE employee ADD CONSTRAINT FK_5D9F75A1708A0E0 FOREIGN KEY (gender_id) REFERENCES gender (id)');
        $this->addSql('ALTER TABLE employee ADD CONSTRAINT FK_5D9F75A11C9DA55 FOREIGN KEY (nationality_id) REFERENCES nationality (id)');
        $this->addSql('ALTER TABLE employee ADD CONSTRAINT FK_5D9F75A131FFB0CB FOREIGN KEY (nationalityType_id) REFERENCES nationality_type (id)');
        $this->addSql('ALTER TABLE employee ADD CONSTRAINT FK_5D9F75A12608C31C FOREIGN KEY (positionRoles_id) REFERENCES position_role (id)');
        $this->addSql('ALTER TABLE area ADD CONSTRAINT FK_D7943D688BAC62AF FOREIGN KEY (city_id) REFERENCES city (id)');
        $this->addSql('ALTER TABLE machine ADD CONSTRAINT FK_1505DF843AE7FDC9 FOREIGN KEY (machineType_id) REFERENCES machine_type (id)');
        $this->addSql('ALTER TABLE receipt ADD CONSTRAINT FK_5399B6457294869C FOREIGN KEY (article_id) REFERENCES article (id)');
        $this->addSql('ALTER TABLE receipt ADD CONSTRAINT FK_5399B645F6B75B26 FOREIGN KEY (machine_id) REFERENCES machine (id)');
        $this->addSql('ALTER TABLE receipt ADD CONSTRAINT FK_5399B6458C03F15C FOREIGN KEY (employee_id) REFERENCES employee (id)');
        $this->addSql('ALTER TABLE article ADD CONSTRAINT FK_23A0E666B899279 FOREIGN KEY (patient_id) REFERENCES patient (id)');
        $this->addSql('ALTER TABLE article ADD CONSTRAINT FK_23A0E66537048AF FOREIGN KEY (transfer_id) REFERENCES transfer (id)');
        $this->addSql('ALTER TABLE article ADD CONSTRAINT FK_23A0E66F6B75B26 FOREIGN KEY (machine_id) REFERENCES machine (id)');
        $this->addSql('ALTER TABLE article ADD CONSTRAINT FK_23A0E66AE80F5DF FOREIGN KEY (department_id) REFERENCES department (id)');
        $this->addSql('ALTER TABLE disability ADD CONSTRAINT FK_413A5CB27294869C FOREIGN KEY (article_id) REFERENCES article (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE patient DROP FOREIGN KEY FK_1ADAD7EB31FFB0CB');
        $this->addSql('ALTER TABLE employee DROP FOREIGN KEY FK_5D9F75A131FFB0CB');
        $this->addSql('ALTER TABLE article DROP FOREIGN KEY FK_23A0E666B899279');
        $this->addSql('ALTER TABLE receipt DROP FOREIGN KEY FK_5399B6458C03F15C');
        $this->addSql('ALTER TABLE patient DROP FOREIGN KEY FK_1ADAD7EB708A0E0');
        $this->addSql('ALTER TABLE employee DROP FOREIGN KEY FK_5D9F75A1708A0E0');
        $this->addSql('ALTER TABLE patient DROP FOREIGN KEY FK_1ADAD7EB1C9DA55');
        $this->addSql('ALTER TABLE employee DROP FOREIGN KEY FK_5D9F75A11C9DA55');
        $this->addSql('ALTER TABLE receipt DROP FOREIGN KEY FK_5399B645F6B75B26');
        $this->addSql('ALTER TABLE article DROP FOREIGN KEY FK_23A0E66F6B75B26');
        $this->addSql('ALTER TABLE employee DROP FOREIGN KEY FK_5D9F75A12608C31C');
        $this->addSql('ALTER TABLE article DROP FOREIGN KEY FK_23A0E66537048AF');
        $this->addSql('ALTER TABLE machine DROP FOREIGN KEY FK_1505DF843AE7FDC9');
        $this->addSql('ALTER TABLE employee DROP FOREIGN KEY FK_5D9F75A1AE80F5DF');
        $this->addSql('ALTER TABLE article DROP FOREIGN KEY FK_23A0E66AE80F5DF');
        $this->addSql('ALTER TABLE financial_aid DROP FOREIGN KEY FK_4704BE4F7294869C');
        $this->addSql('ALTER TABLE receipt DROP FOREIGN KEY FK_5399B6457294869C');
        $this->addSql('ALTER TABLE disability DROP FOREIGN KEY FK_413A5CB27294869C');
        $this->addSql('ALTER TABLE area DROP FOREIGN KEY FK_D7943D688BAC62AF');
        $this->addSql('DROP TABLE nationality_type');
        $this->addSql('DROP TABLE financial_aid');
        $this->addSql('DROP TABLE patient');
        $this->addSql('DROP TABLE employee');
        $this->addSql('DROP TABLE area');
        $this->addSql('DROP TABLE gender');
        $this->addSql('DROP TABLE nationality');
        $this->addSql('DROP TABLE machine');
        $this->addSql('DROP TABLE receipt');
        $this->addSql('DROP TABLE position_role');
        $this->addSql('DROP TABLE transfer');
        $this->addSql('DROP TABLE machine_type');
        $this->addSql('DROP TABLE department');
        $this->addSql('DROP TABLE phone_number');
        $this->addSql('DROP TABLE article');
        $this->addSql('DROP TABLE city');
        $this->addSql('DROP TABLE disability');
    }
}
