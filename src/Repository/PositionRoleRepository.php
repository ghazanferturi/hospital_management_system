<?php

namespace App\Repository;

use App\Entity\PositionRole;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method PositionRole|null find($id, $lockMode = null, $lockVersion = null)
 * @method PositionRole|null findOneBy(array $criteria, array $orderBy = null)
 * @method PositionRole[]    findAll()
 * @method PositionRole[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PositionRoleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PositionRole::class);
    }

    // /**
    //  * @return PositionRole[] Returns an array of PositionRole objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PositionRole
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
