<?php

namespace App\Repository;

use App\Entity\FinancialAid;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method FinancialAid|null find($id, $lockMode = null, $lockVersion = null)
 * @method FinancialAid|null findOneBy(array $criteria, array $orderBy = null)
 * @method FinancialAid[]    findAll()
 * @method FinancialAid[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FinancialAidRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FinancialAid::class);
    }

    // /**
    //  * @return FinancialAid[] Returns an array of FinancialAid objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FinancialAid
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
