<?php

namespace App\Repository;

use App\Entity\NationalityType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method NationalityType|null find($id, $lockMode = null, $lockVersion = null)
 * @method NationalityType|null findOneBy(array $criteria, array $orderBy = null)
 * @method NationalityType[]    findAll()
 * @method NationalityType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NationalityTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, NationalityType::class);
    }

    // /**
    //  * @return NationalityType[] Returns an array of NationalityType objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('n.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?NationalityType
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
